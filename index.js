// express and nunjucks templating
const express = require('express');
const expressNunjucks = require('express-nunjucks');
const app = express();

//react framework
const react = require('react');

// https://stackoverflow.com/questions/5710358/how-to-retrieve-post-query-parameters/12008719#12008719
var bodyParser = require('body-parser');
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

// dev environment
const isDev = app.get('env') === 'development';

// couchdb
var nano = require('nano')('http://localhost:5984');

// settings var 
var settings = {};
settings.ALLOW_LOGIN = true;
settings.HTML_ONLY = true;
settings.DOMAIN = 'localhost';
settings.EMAIL = 'love@windysurf.com'
settings.COUCHDB_PREFIX = 'windysurf_';
settings.LOCAL = true;
settings.PORT = 36364;
settings.NAME = 'WindySurf';
settings.FAKE_INSERT = false;
module.exports = settings;

app.set('views', __dirname + '/views');
app.use('/static', express.static('public'))

const njk = expressNunjucks(app, {
    watch: isDev,
    noCache: isDev 
});

// database of individual checkins
var checkins = nano.db.create(settings.COUCHDB_PREFIX+'checkins');
var checkins = nano.use(settings.COUCHDB_PREFIX+'checkins');

// database of expired checkins
var checkins_expired = nano.db.create(settings.COUCHDB_PREFIX+'checkins_expired');
var checkins_expired = nano.use(settings.COUCHDB_PREFIX+'checkins_expired');

// cover landing page with the waiver and signature requirement
var profiles = nano.db.create(settings.COUCHDB_PREFIX+'profiles');
var profiles = nano.use(settings.COUCHDB_PREFIX+'profiles');

/////////////////////////////////////////////////////////////////////////////////

// list of news or text boxes on the cover page
var eternities = nano.db.create(settings.COUCHDB_PREFIX+'eternities');
var eternities = nano.use(settings.COUCHDB_PREFIX+'eternities');

var eternities_each = [];

eternities.list(function(err, body) {
  if (!err) {
    console.log('hi eternities loop')
    body.rows.forEach(function(doc) {
        console.log(doc.id);
        eternities.get(doc.id, function(err,eternity) {
            eternities_each.push(eternity);
        });
    });
  } else {
      console.log("error", err);
  }
});

/////////////////////////////////////////////////////////////////////////////////


// comparisons of different eternities providers like calico labs
var comparisons = nano.db.create(settings.COUCHDB_PREFIX+'comparisons')
var comparisons = nano.use(settings.COUCHDB_PREFIX+'comparisons');
var comparisons_each = [];

comparisons.list(function(err, body) {
  if (!err) {
    console.log('hi comparisons loop')
    body.rows.forEach(function(doc) {
        console.log(doc.id);
        comparisons.get(doc.id, function(err,provider) {
            comparisons_each.push(provider);
        });
    });
  } else {
      console.log("error", err);
  }
});

/////////////////////////////////////////////////////////////////////////////////
//                      fake insert as a test
/////////////////////////////////////////////////////////////////////////////////

if (settings.LOCAL && settings.FAKE_INSERT) {
    eternities.insert({ title: "hi", message: "message", author: "author" }, function(err, body, header) {
      if (err) {
        console.log('[.insert] ', err.message);
      } else {
          console.log('you have added to the eternity.', body)
      }
    });
} //endif


/////////////////////////////////////////////////////////////////////////////////

app.get('/', (req, res) => {
    res.render('waiver', {settings: settings, waiver: true});
});

app.post('/api/checkin_search/', (req, res) => {
    console.log('/api/checkin_search/: pass in lat and lon' + req.body);
    console.log('/api/checkin_search/: pass in lat and lon, lat' + req.body.lat);
    console.log('/api/checkin_search/: pass in lat and lon, lon' + req.body.lon);
    console.log('/api/checkin_search/: pass in lat and lon, profile_id' + req.body.profile_id);
    var body_id = false;
    var body_rev = false;
    var unix = Math.round(+new Date()/1000);
        checkins.insert(req.body, function(err, body, header) {
          if (err) {
            console.log('[.insert] checkins error via checkin', err.message);
            res.statusMessage = "Error: " + err.message;
            res.status(400).end();  
          } else {
              console.log('you have added to the checkins.', body);
              console.log('you have added to the checkins. body.id', body.id);
              body_id = body.id;
              body_rev = body.rev;
              res.send({status: 'checkedin', checkin_id: body_id, checkin_rev: body_rev});
          }
        });
});

app.get('/api/proximity/', (req, res) => {
    console.log('/api/proximity/: profile_id ' + req.body.profile_id);
    console.log('/api/proximity/: lat ' + req.body.lat);
    console.log('/api/proximity/: lon ' + req.body.lon);
    
    // get req
    var profile_id = req.body.profile_id;
    var lat = req.body.lat;
    var lon = req.body.lon;
    
    // current timestamp
    var unix = Math.round(+new Date()/1000);
    
    // return data
    var checkins = nano.use(settings.COUCHDB_PREFIX+'checkins');
    var checkins_each = [];
    checkins.list(function(err, body) {
        if (!err) {
            console.log('hi proximity loop')
            body.rows.forEach(function(doc) {
                console.log(doc.id);
                checkins.get(doc.id, function(err,jsondoc) {
                    console.log(JSON.stringify(jsondoc));
                    if (jsondoc.profile_id != profile_id) {
                        console.log('appending checkin');
                        checkins_each.push(jsondoc);
                    }
                        
                });
            });
            res.send({status: 'proximity', checkins: checkins_each});
        } else {
            console.log("error", err);
            res.send({status: 'fail', error: err});
            
        }
    });
    
    
    
});

app.post('/api/checkin/', (req, res) => {
    console.log('/api/checkin/: ' + req.body.signature);
    var body_id = false;
    var body_rev = false;
    var unix = Math.round(+new Date()/1000);
        checkins.insert(req.body, function(err, body, header) {
          if (err) {
            console.log('[.insert] checkins error via checkin', err.message);
            res.statusMessage = "Error: " + err.message;
            res.status(400).end();  
          } else {
              console.log('you have added to the checkins.', body);
              console.log('you have added to the checkins. body.id', body.id);
              body_id = body.id;
              body_rev = body.rev;
              res.send({status: 'checkedin', checkin_id: body_id, checkin_rev: body_rev});
          }
        });
});

app.post('/api/waiver/', (req, res) => {
    console.log('/api/waiver/: ' + req.body.signature);
        var body_id = false;
        var body_rev = false;
        var unix = Math.round(+new Date()/1000);
        profiles.insert({ signature: req.body.signature, date_string: req.body.date, date_timestamp: unix }, function(err, body, header) {
          if (err) {
            console.log('[.insert] profiles error via waiver', err.message);
            res.statusMessage = "Error: " + err.message;
            res.status(400).end();  
          } else {
              console.log('you have added to the profiles via waivers.', body);
              console.log('you have added to the profiles via waivers. body.id', body.id);
              body_id = body.id;
              body_rev = body.rev;
              console.log('you have added to the profiles via waivers. body_id', body_id);
              res.send({status: 'waived', profile_id: body_id, profile_rev: body_rev});
          }
        });
});

app.get('/news', (req, res) => {
    res.render('news', {
        eternities: eternities_each,
        settings: settings,
        waiver: false
    });
});

app.get('/compare', (req, res) => {
    res.render('compare', {
        comparisons: comparisons_each,
        settings: settings,
        waiver: false
    });
});

// the exclusion site
app.get('/yivwiy.html', (req, res) => {
    res.render('exclusion', {
        settings: settings,
        waiver: false
    });
});

app.listen(settings.PORT);

